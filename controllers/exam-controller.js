app.controller("examController", function ($scope, $timeout) {
    $scope.currentQuestionIndex = 0;
    $scope.submittedAnswer = '';
    $scope.questionIsAnswered = false;
    $scope.score = 0;

    $scope.isExamOver = false;

    $scope.questions = [{
        question: "which directive do we use to initialize an angular app?",
        answer: "ng-app",
        options: ["ng-init", "ng-app", "ng-begin", "ng-start"]
    }, {
        question: "What is the correct syntax to write an expression?",
        answer: "{{expression}}",
        options: ["[[expression]]", "{expression}", "[expression]", "{{expression}}"]
    }, {
        question: "Which directive binds the values of application data to HTML input controls in angular JS?",
        answer: "ng-model",
        options: ["ng-controller", "ng-model", "ng-bind", "ng-control"]
    }, {
        question: "Which of the following is a filter in Angular Js?",
        answer: "all of the above",
        options: ["currency", "date", "lowercase", "all of the above"]
    }, {
        question: "What is data binding in angular JS?",
        answer: "Synchronization between model part and view part",
        options: ["Synchronization between controller part and view part", "Synchronization between model part and view part", "Synchronization between model part and controller part"]
    }, {
        question: "Which directive is used to specify a controller in HTML element?",
        answer: "ng-controller",
        options: ["ng-controller", "ng-control", "ng-ctrl", "controller"]
    }, {
        question: "Which sign is used as prefix for the built-in objects in AngularJS",
        answer: "$",
        options: ["#", "*", "$", "@"]
    }, {
        question: "Which directive display view for various routes?",
        answer: "ng-view",
        options: ["ng-display", "ng-view", "ng-model", "ng-show"]
    }, {
        question: "Which service is used to communicate with remote server?",
        answer: "$http",
        options: ["$scope", "$rootScope", "$http", "$get"]
    }, {
        question: "Which directive is used to iterate over an array?",
        answer: "ng-repeat",
        options: ["ng-iterate", "ng-traverse", "ng-array", "ng-repeat"]
    }, {
        question: "What is the ng-show directive used for?",
        answer: "displaying based on a condition",
        options: ["showing the data", "displaying based on a condition", "showing the code", "none of the above"]
    }];

    $scope.submitAnswer = function (answer) {
        var correctAnswer = $scope.questions[$scope.currentQuestionIndex].answer;
        $scope.questionIsAnswered = true;
        $scope.submittedAnswer = answer;
        if (answer === correctAnswer) {
            $scope.score += 10;
        }
        $timeout(function () {
            $scope.questionIsAnswered = false;
            $scope.submittedAnswer = '';
            $scope.currentQuestionIndex += 1;

            if ($scope.currentQuestionIndex === $scope.questions.length) {
                $scope.isExamOver = true;
            }
        }, 1000)
    }
})